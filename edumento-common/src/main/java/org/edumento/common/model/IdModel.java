package org.edumento.common.model;

import java.util.UUID;

public class IdModel {
	private final UUID id;

	public IdModel(UUID id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public UUID getId() {
		return id;
	}

}
