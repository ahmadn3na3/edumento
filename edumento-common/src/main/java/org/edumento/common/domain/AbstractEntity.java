package org.edumento.common.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import io.micronaut.data.annotation.event.PrePersist;
import io.micronaut.data.annotation.event.PreUpdate;

@MappedSuperclass
public abstract class AbstractEntity {
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	@Column
	private String createBy;

	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModifiedDate = null;

	@Column
	private String lastModifiedBy;
	

	protected AbstractEntity() {
	}
	
	@PrePersist
	void preSave() {
		this.creationDate = new Date();
		this.createBy = "User";
	}
	
	@PreUpdate
	void preUpdate() {
		this.lastModifiedDate = new Date();
		this.lastModifiedBy = "User";
	}

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return the createBy
	 */
	public String getCreateBy() {
		return createBy;
	}

	/**
	 * @param createBy the createBy to set
	 */
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	/**
	 * @return the lastModifiedDate
	 */
	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	/**
	 * @param lastModifiedDate the lastModifiedDate to set
	 */
	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	/**
	 * @return the lastModifiedBy
	 */
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	/**
	 * @param lastModifiedBy the lastModifiedBy to set
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

}
