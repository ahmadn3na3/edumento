package org.edumento.space.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.edumento.common.model.IdModel;
import org.edumento.space.domain.Space;

public interface SpaceService {

	Optional<Space> get(UUID id);
	List<Space> get(Integer page,Integer pageLimit);
	IdModel create(Space space);
	void update(Space space, UUID id);
	void delete(UUID id);
	
	
	
}
