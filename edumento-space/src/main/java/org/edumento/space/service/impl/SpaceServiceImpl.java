package org.edumento.space.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.edumento.common.model.IdModel;
import org.edumento.space.domain.Space;
import org.edumento.space.repos.SpaceRepo;
import org.edumento.space.service.SpaceService;

import io.micronaut.core.util.StringUtils;
import io.micronaut.data.model.Pageable;
import jakarta.inject.Singleton;

@Singleton
public class SpaceServiceImpl implements SpaceService {

	private final SpaceRepo spaceRepos;

	public SpaceServiceImpl(SpaceRepo repos) {
		this.spaceRepos = repos;
	}

	@Override
	public Optional<Space> get(UUID id) {
		return spaceRepos.findById(id);
	}

	@Override
	public List<Space> get(Integer page, Integer pageLimit) {
		return spaceRepos.findAll(Pageable.from(page, pageLimit)).getContent();
	}

	@Override
	public IdModel create(Space space) {
		if (StringUtils.isEmpty(space.getName())) {
			throw new RuntimeException("Name can not be empty");
		}
		return new IdModel(spaceRepos.save(space).getId());
	}

	@Override
	public void update(Space space, UUID id) {
		if (this.spaceRepos.existsById(id)) {
			return;
		}
		space.setId(id);
		spaceRepos.update(space);
	}

	@Override
	public void delete(UUID id) {
		spaceRepos.deleteById(id);
	}
	
	

}
