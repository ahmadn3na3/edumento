package org.edumento.space.resources;

import java.util.List;
import java.util.Optional;

import org.edumento.common.model.IdModel;
import org.edumento.space.domain.Space;
import org.edumento.space.service.SpaceService;

import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.QueryValue;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import jakarta.inject.Inject;

@Controller("/api/space")
public class SpaceResource {

	@Inject
	SpaceService spaceService;

	@Get("/hello")
	public String hello() {
		return "hello there";
	}

	@Post
	public IdModel createSpace(@Body Space space) {
		return spaceService.create(space);
	}

	@Get
	public List<Space> get(@QueryValue("page") Optional<Integer> page,
			@QueryValue("page_size") Optional<Integer> pageSize) {
		return spaceService.get(page.orElse(0), page.orElse(10));

	}

}
