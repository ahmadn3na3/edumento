package org.edumento.space.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.edumento.common.domain.AbstractEntity;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import io.micronaut.data.annotation.GeneratedValue;

/**
 * @author ahmad
 * 
 *
 */
@DynamicInsert
@DynamicUpdate
@Table(indexes = { @Index(columnList = "name", name = "name_index"),
		@Index(columnList = "description", name = "description_index") })
@Entity
public class Space extends AbstractEntity {
	@Id
	@GeneratedValue()
	private UUID id = UUID.randomUUID();
	@Column
	private String color;
	@Column(nullable = false)
	private String name;
	@Column
	private Double rating;
	@Column
	private Double price;
	@Column
	private Boolean paid = Boolean.FALSE;
	@Column
	private Boolean isPrivate = Boolean.FALSE;
	@Column
	private String image;
	@Column
	private String thumbnail;
	@Column
	private String chatRoomId;

	@Column(length = 600)
	private String description;

	@Column
	private Boolean joinRequestsAllowed = Boolean.FALSE;
	@Column
	private Boolean autoWifiSyncAllowed = Boolean.FALSE;
	@Column
	private Boolean showCommunity = Boolean.FALSE;

	@Column
	private Boolean allowLeave = Boolean.TRUE;

	@Column
	private Boolean allowRecommendation = Boolean.FALSE;

	public Space() {
	}

	/**
	 * @return the id
	 */
	public UUID getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(UUID id) {
		this.id = id;
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the rating
	 */
	public Double getRating() {
		return rating;
	}

	/**
	 * @param rating the rating to set
	 */
	public void setRating(Double rating) {
		this.rating = rating;
	}

	/**
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(Double price) {
		this.price = price;
	}

	/**
	 * @return the paid
	 */
	public Boolean getPaid() {
		return paid;
	}

	/**
	 * @param paid the paid to set
	 */
	public void setPaid(Boolean paid) {
		this.paid = paid;
	}

	/**
	 * @return the isPrivate
	 */
	public Boolean getIsPrivate() {
		return isPrivate;
	}

	/**
	 * @param isPrivate the isPrivate to set
	 */
	public void setIsPrivate(Boolean isPrivate) {
		this.isPrivate = isPrivate;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * @return the thumbnail
	 */
	public String getThumbnail() {
		return thumbnail;
	}

	/**
	 * @param thumbnail the thumbnail to set
	 */
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	/**
	 * @return the chatRoomId
	 */
	public String getChatRoomId() {
		return chatRoomId;
	}

	/**
	 * @param chatRoomId the chatRoomId to set
	 */
	public void setChatRoomId(String chatRoomId) {
		this.chatRoomId = chatRoomId;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the joinRequestsAllowed
	 */
	public Boolean getJoinRequestsAllowed() {
		return joinRequestsAllowed;
	}

	/**
	 * @param joinRequestsAllowed the joinRequestsAllowed to set
	 */
	public void setJoinRequestsAllowed(Boolean joinRequestsAllowed) {
		this.joinRequestsAllowed = joinRequestsAllowed;
	}

	/**
	 * @return the autoWifiSyncAllowed
	 */
	public Boolean getAutoWifiSyncAllowed() {
		return autoWifiSyncAllowed;
	}

	/**
	 * @param autoWifiSyncAllowed the autoWifiSyncAllowed to set
	 */
	public void setAutoWifiSyncAllowed(Boolean autoWifiSyncAllowed) {
		this.autoWifiSyncAllowed = autoWifiSyncAllowed;
	}

	/**
	 * @return the showCommunity
	 */
	public Boolean getShowCommunity() {
		return showCommunity;
	}

	/**
	 * @param showCommunity the showCommunity to set
	 */
	public void setShowCommunity(Boolean showCommunity) {
		this.showCommunity = showCommunity;
	}

	/**
	 * @return the allowLeave
	 */
	public Boolean getAllowLeave() {
		return allowLeave;
	}

	/**
	 * @param allowLeave the allowLeave to set
	 */
	public void setAllowLeave(Boolean allowLeave) {
		this.allowLeave = allowLeave;
	}

	/**
	 * @return the allowRecommendation
	 */
	public Boolean getAllowRecommendation() {
		return allowRecommendation;
	}

	/**
	 * @param allowRecommendation the allowRecommendation to set
	 */
	public void setAllowRecommendation(Boolean allowRecommendation) {
		this.allowRecommendation = allowRecommendation;
	}

}
