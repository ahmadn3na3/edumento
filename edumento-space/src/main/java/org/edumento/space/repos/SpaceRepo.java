package org.edumento.space.repos;

import java.util.UUID;

import org.edumento.space.domain.Space;

import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.PageableRepository;

@Repository
public interface SpaceRepo extends PageableRepository<Space, UUID>{
	

}
