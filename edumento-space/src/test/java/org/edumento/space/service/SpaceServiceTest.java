package org.edumento.space.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.UUID;

import org.edumento.space.domain.Space;
import org.junit.jupiter.api.Test;

import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;

@MicronautTest
public class SpaceServiceTest {

	@Inject
	SpaceService service;

	@Test
	void testCreate() {
		var space = new Space();
		space.setName("testName");
		var idModel = service.create(space);
		assertNotNull(idModel);
		assertNotNull(idModel.getId());

	}

	@Test
	void testCreateWithEmptyName() {
		var space = new Space();
		var e = assertThrows(RuntimeException.class, () -> {
			service.create(space);
		});
		assertNotNull(e);
		assertTrue(e.getMessage().contains("Name can not be empty"));
	}

	@Test
	void testGetById() {
		var space = new Space();
		space.setName("name1");
		var id = service.create(space);
		assertNotNull(id);
		assertTrue(service.get(id.getId()).isPresent());

	}

	@Test
	void testGetByInvalid() {
		assertFalse(service.get(UUID.randomUUID()).isPresent());

	}

	
	@Test
	void testGet() {
		var space = new Space();
		space.setName("name1");
		service.create(space);
		space = new Space();
		space.setName("name2");
		service.create(space);
		var spaces = service.get(0, 10);
		assertEquals(2, spaces.size());

	}

	@Test
	void testDelete() {
		var space = new Space();
		space.setName("name1");
		var id = service.create(space);

		var spaces = service.get(0, 10);
		assertEquals(1, spaces.size());
		service.delete(id.getId());
		spaces = service.get(0, 10);
		assertEquals(0, spaces.size());
	}
	@Test
	void testDeleteByInvalid() {
		service.delete(UUID.randomUUID());
	}
	
	@Test
	void testUpdate() {
		var space = new Space();
		space.setName("testName");
		var idModel = service.create(space);
		space = new Space();
		space.setName("testName");
		space.setDescription("testDescription");
		service.update(space, idModel.getId());
		
	}
	

}
